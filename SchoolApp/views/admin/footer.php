<!-- footer content -->
<footer>
	<div class="pull-right">
		&copy; LMS <?php echo date('Y'); ?> All Rights Reserved.
	</div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/js//bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/js//fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url();?>assets/js/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo base_url();?>assets/js/Chart.min.js"></script>
<!-- gauge.js -->
<script src="<?php echo base_url();?>assets/js/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script
	src="<?php echo base_url();?>assets/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/js/icheck.min.js"></script>
<!-- Skycons -->
<script src="<?php echo base_url();?>assets/js/skycons.js"></script>
<!-- Flot -->
<script src="<?php echo base_url();?>assets/js/jquery.flot.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.flot.pie.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.flot.time.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.flot.stack.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?php echo base_url();?>assets/js/jquery.flot.orderBars.js"></script>
<script
	src="<?php echo base_url();?>assets/js/jquery.flot.spline.min.js"></script>
<script src="<?php echo base_url();?>assets/js/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?php echo base_url();?>assets/js/date.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url();?>assets/js/jquery.vmap.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.vmap.world.js"></script>
<script
	src="<?php echo base_url();?>assets/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/js/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
</body>
</html>