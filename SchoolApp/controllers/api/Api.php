<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/**
 * @author Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : This controller serve the request from mobile application
 */
class Api extends REST_Controller {
    public function __construct() {
    	
        parent::__construct();
        $this->load->model('api/Api_service');
        include_once './application/objects/Response.php';
        $this->load->helper('string');
        $this->load->library(array('form_validation','excel'));
    }
    public function index_get(){
    	$x = "test data";
    	$this->set_response(array('status' => '1','message' => 'test', 'jsonData'=> $x) , REST_Controller::HTTP_OK);
    }
    /**
     * @author : Anjani Kumar Gupta
     * Date: 10th Nov 2016
     * Method: login
     * Description: validate credentials
     */
    public function login_post()
    {
    	if($_SERVER['REQUEST_METHOD']== "POST"){
    		
    		$email = $this->post('email');
    		$password = $this->post('password');
    		$deviceType = $this->post('deviceType');
    		$deviceId = $this->post('deviceId');
    		$fcmRegId = $this->post('fcmRegId');
    		
    		try {
    			
    			$this->form_validation->set_rules('email', 'Email', 'trim|required');
    			$this->form_validation->set_rules('password', 'Password', 'trim|required');
    			$this->form_validation->set_rules('deviceType', 'Device Type Confirmation', 'trim|required');
    			$this->form_validation->set_rules('deviceId', 'Device Id', 'trim|required');
    			$this->form_validation->set_rules('fcmRegId', 'FCM regitration Id', 'trim|required');
    			
    			if( $this->form_validation->run() == TRUE ){
    				$apiService = new Api_service();
    				$response = $apiService->login($email,$password,$deviceType,$deviceId,$fcmRegId);
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    			}else{
    				$this->set_response(array('status' => 0,'message' => 'Requested data not found'), REST_Controller::HTTP_OK);
    			}
    			
    		} catch (Exception $e) {
    			$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    		}
    		
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    	}
    }
  
    /**
     * @author : Anjani Kumar Gupta
     * Date: 12th Nov 2016
     * Method: addLeads
     * Description: add leads
     */
    public function addLeads_post(){
    	if($_SERVER['REQUEST_METHOD']== "POST"){
    		$apiService = new Api_service();
    		$file = $apiService->picture();  //print_r($file); die();
    		
    		$filename = $file['upload_data']['file_name'];  
    		$data['name'] = $this->post('name');
    		$data['mobile'] = $this->post('mobile');
    		$data['category_id'] = $this->post('category_id');
    		$data['product_id'] = $this->post('product_id');
    		$data['branch'] = $this->post('branch');
    		$data['cluster'] = $this->post('cluster');
    		$data['sourcedBy'] = $this->post('sourcedBy');
    		$data['address'] = $this->post('address');
    		$data['emailId'] = $this->post('emailId');
    		$data['isExistingCustomer'] = $this->post('isExistingCustomer');
    		$data['cutomerId'] = $this->post('cutomerId');
    		$data['businessCard'] = $filename;
    		$data['added_by'] = $this->post('added_by');
    		$data['follower_id'] = $this->post('follower_id');
    		$data['status'] = $this->post('status');
    		try {
    			
    			$this->form_validation->set_rules('name', 'Name', 'trim|required');
    			$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
    			$this->form_validation->set_rules('category_id', 'Category', 'trim|required');
    			$this->form_validation->set_rules('product_id', 'Product', 'trim|required');
    			$this->form_validation->set_rules('branch', 'Branch', 'trim|required');
    			$this->form_validation->set_rules('cluster', 'Cluster', 'trim|required');
    			$this->form_validation->set_rules('sourcedBy', 'Sourced by', 'trim|required');
    			$this->form_validation->set_rules('address', 'Address', 'trim|required');
    			$this->form_validation->set_rules('emailId', 'Email Id', 'trim|required');
    			$this->form_validation->set_rules('isExistingCustomer', 'Is existing customer', 'trim|required');
    			$this->form_validation->set_rules('cutomerId', 'Customer', 'trim|required');
    			$this->form_validation->set_rules('added_by', 'Added by', 'trim|required');
    			$this->form_validation->set_rules('status', 'Status', 'trim|required');
    			
    			if( $this->form_validation->run() == TRUE ){
    				
    				$response = $apiService->addLeads($data);
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg()) , REST_Controller::HTTP_OK);
    				}
    			}else{
    				$this->set_response(array('status' => 0,'message' => 'Requested data not found'), REST_Controller::HTTP_OK);
    			}
    			
    		} catch (Exception $e) {
    			$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    		}
    		
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    	}
    }
    
    public function category_get(){
    	if($_SERVER['REQUEST_METHOD']== "GET"){
    		try {
    				$apiService = new Api_service();
    				$response = $apiService->getCategory();
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    			
    		} catch (Exception $e) {
    			$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    		}
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    	}
    }
    
    public function product_get(){
    	if($_SERVER['REQUEST_METHOD']== "GET"){
    		$catId = $this->get('catId'); 
    	
    		try {
    			
    			if( $catId ){
	    			$apiService = new Api_service();
	    			$response = $apiService->getProduct($catId);
	    			if($response->getStatus()){
	    				$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
	    			}else{
	    				$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
	    			}
    			}else{
    				$this->set_response(array('status' => 0,'message' => 'Requested data not found'), REST_Controller::HTTP_OK);
    			}
    			
    			 
    		} catch (Exception $e) {
    			$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    		}
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    	}
    }
    
    public function cluster_get(){
    	if($_SERVER['REQUEST_METHOD']== "GET"){
    		
    		try {
    			 
    				$apiService = new Api_service();
    				$response = $apiService->getCluster();
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    			
    		} catch (Exception $e) {
    			$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    		}
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    	}
    }
    public function branch_get(){
    	if($_SERVER['REQUEST_METHOD']== "GET"){
    		$clusterId = $this->get('clusterId');
    		 
    		try {
    			 
    			if( $clusterId ){
    				$apiService = new Api_service();
    				$response = $apiService->getBranch($clusterId);
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    			}else{
    				$this->set_response(array('status' => 0,'message' => 'Requested data not found'), REST_Controller::HTTP_OK);
    			}
    		} catch (Exception $e) {
    			$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    		}
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    	}
    }
/**
     * @author : Nishant Singh
     * Date: 21th Nov 2016
     * Method: addMeeting
     * Description: add meetings
     */
    public function addMeeting_post(){
    	if($_SERVER['REQUEST_METHOD']== "POST")
    	{
    		$data['title'] = $this->post('title');
    		$data['meetingTime'] = $this->post('meetingTime');
    		$data['description'] = $this->post('description');
    		$data['user_id'] = $this->post('user_id');
    		$data['status'] = $this->post('status');
    		$data['modificationDate'] = $this->post('modificationDate');
    		$data['creationDate'] = $this->post('creationDate');
    		//print_r($data); die();
    		try {
    			
    				$apiService = new Api_service();
    				$response = $apiService->addMeeting($data);
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg()) , REST_Controller::HTTP_OK);
    				}
    			
    		} catch (Exception $e) {
    			$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    		}
    		
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    	}
    }
    /**
     * @author : Nishant Singh
     * Date: 23th Nov 2016
     * Method: updateLeadDetails
     * Description: modify the details
     */
    public function updateLeadDetails_post(){
    	
    	if($_SERVER['REQUEST_METHOD']== "POST"){
    		$apiService = new Api_service();
    		$file = $apiService->picture(); 
    		
    		$filename = $file['upload_data']['file_name'];
    		$data=$this->post();
    		unset($data['0']);
    		$id=$this->input->post('id');
    		$data['businessCard'] = $filename;
    		try {
    				$response = $apiService->updateLeadDetails($data,$id);
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg()) , REST_Controller::HTTP_OK);
    				}
    			} catch (Exception $e) {
    			$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    		}
    		
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    	}
    
    	} 
    	/**
    	 * @author : Nishant Singh
    	 * Date: 21th Nov 2016
    	 * Method: status
    	 * Description: get all status
    	 */
    	public function status_get(){
    		if($_SERVER['REQUEST_METHOD']== "GET"){
    			try {
    				$apiService = new Api_service();
    				$response = $apiService->getStatus();
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    					
    			} catch (Exception $e) {
    				$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    			}
    		}else{
    			$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    		}
    	}
    	/**
    	 * @author : Nishant Singh
    	 * Date: 21th Nov 2016
    	 * Method: changeStatus
    	 * Description: change status 
    	 */
    	public function changeStatus_post(){
    		if($_SERVER['REQUEST_METHOD']== "POST"){
    			$data['leadId'] = $this->post('leadId');
    			$data['statusId'] = $this->post('statusId');
    			try {
    				$apiService = new Api_service();
    				$response = $apiService->changeStatus($data);
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    				 
    			} catch (Exception $e) {
    				$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    			}
    		}else{
    			$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    		}
    	}
    	/**
    	 * @author : Nishant Singh
    	 * Date: 21th Nov 2016
    	 * Method: getAllMeeting
    	 * Description: see all the details of meeting
    	 */
    	public function getAllMeeting_get(){
    		
    		if($_SERVER['REQUEST_METHOD']== "GET"){
    			try {
    				$data['userId'] = $this->post('userId');
    				$apiService = new Api_service();
    				$response = $apiService->getAllMeeting();
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    					
    			} catch (Exception $e) {
    				$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    			}
    		}else{
    			$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    		}
    	}	
    	/**
    	 * @author : Nishant Singh
    	 * Date: 22th Nov 2016
    	 * Method: getLeadsSummary
    	 * Description: see all the details of leads
    	 */
    	public function getLeadsSummary_get(){
    		if($_SERVER['REQUEST_METHOD']== "GET"){
    			try {
    				$data['userId'] = $this->post('userId');
    				$apiService = new Api_service();
    				$response = $apiService->getLeadsSummary();
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    					
    			} catch (Exception $e) {
    				$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    			}
    		}else{
    			$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    		}
    	}
    	/**
    	 * @author : Nishant Singh
    	 * Date: 22th Nov 2016
    	 * Method: getProcessedSummary
    	 * Description: see all the details of leads
    	 */
    	public function getProcessedSummary_get(){
    		if($_SERVER['REQUEST_METHOD']== "GET"){
    			try {
    				$data['userId'] = $this->post('userId');
    				$apiService = new Api_service();
                    $response = $apiService->getProcessedSummary();
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    					
    			} catch (Exception $e) {
    				$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    			}
    		}else{
    			$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    		}
    	}
    	/**
    	 * @author : Nishant Singh
    	 * Date: 23th Nov 2016
    	 * Method: transferLead
    	 * Description: details of all product catageory
    	 */
    	/* public function transferLead_post(){
    		if($_SERVER['REQUEST_METHOD']== "POST"){
    			try {
    				$apiService = new Api_service();
    				$response = $apiService->transferLead();
    				if($response->getStatus()){
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}else{
    					$this->set_response(array('status' => $response->getStatus(),'message' => $response->getMsg(), 'jsonData'=>$response->getObjArray()) , REST_Controller::HTTP_OK);
    				}
    					
    			} catch (Exception $e) {
    				$this->set_response(array('status' => 0,'message' => $e->getMessage()) , REST_Controller::HTTP_BAD_REQUEST);
    			}
    		}else{
    			$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_METHOD_NOT_ALLOWED);
    		}
    	} */
    	
}
