<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Admin extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'admin/admin/Admin_service' );
		include_once './application/objects/Response.php';
		$this->load->helper ( 'auth' );
		$this->load->library ( 'form_validation' );
	}
	public function index() {
		if (is_loggedin ()) {
			redirect ( 'admin/dashboard' );
		} else {
			$this->load->view ( 'admin/login' );
		}
	}
	public function dashboard() {
		if (is_loggedin ()) {
			$data ['metaData'] = 'yes';
			$data ['title'] = 'LMS | Dashboard';
			$data ['keywords'] = '';
			$data ['description'] = '';
			$this->template->load ( 'admin/dashboard', $data );
			// $this->load->view('admin/dashboard');
		} else {
			redirect ( 'admin' );
		}
	}
	public function forgot_password() {
		$this->load->view ( 'admin/forgot_password' );
	}
	public function reset_password() {
		$email = $this->input->post ( 'email' );
		
		$check = $this->Admin_service->checkEmail ( $email );
		
		if ($check) {
			
			$random_code = mt_rand ( 100000, 999999 ); // echo $random_code;
			$update = $this->Admin_service->updatePassword ( $random_code, $email );
			
			if ($update) {
				
				$response ['status'] = 1;
				$response ['msg'] = "Your new password has been sent to your email.";
			}
			
			$this->send_mail ( $random_code, $email );
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Email id doesn't exist";
		}
		
		echo json_encode ( $response );
		
		// print_r($check); die();
		
		// $this->load->view('admin/forgot_password');
	}
	function send_mail($random_code, $email) {
		error_reporting ( 0 );
		// print_r($value); die();
		// $subject = $value['subject'];
		
		$message = "<b>Hi, </b>" . '</br>';
		$message .= '<b>Your new temporory password is ' . $random_code . ' </b>' . '</br>';
		$message .= '<b>After login you can change your password</b>' . '</br>';
		
		// print_r($subject);
		// print_r($message); //die();
		
		$this->load->library ( 'email' );
		$this->email->set_mailtype ( 'html' );
		$this->email->from ( 'contact@lms.com' );
		$this->email->to ( $email );
		$this->email->subject ( 'forgot Password' );
		$this->email->message ( $message );
		$this->email->send ();
	}
	public function profile() {
		$userid = $this->session->userdata ( 'id' );
		$data ['user'] = $this->Admin_service->GetUserProfile ( $userid );
		// print_r($userid); die();
		// $this->load->view('admin/profile');
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/profile', $data );
	}
	public function update_profile() {
		$data = $this->input->post ( 'user' );
		$userid = $this->session->userdata ( 'id' ); // print_r($data); die();
		$update = $this->Admin_service->update_profile ( $data, $userid );
		
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your Profile has been successfully updated";
			// unset($this->input->post());
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		
		echo json_encode ( $response );
	}
	public function change_password() {
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/change_password', $data );
	}
	public function update_password() {
		$data = $this->input->post ( 'user' );
		$userid = $this->session->userdata ( 'id' );
		if ($data ['password'] != '' || $data ['repassword'] != '') {
			if ($data ['password'] === $data ['repassword']) {
				unset ( $data ['repassword'] );
				$data ['password'] = md5 ( $data ['password'] );
				$update = $this->Admin_service->update_password ( $data, $userid );
				if ($update) {
					$response ['status'] = 1;
					$response ['msg'] = "Your Password has been successfully changed";
				}
			} 

			else {
				$response ['status'] = 0;
				$response ['msg'] = "Password doesn't match , please enter same password";
			}
		} 

		else {
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter the Password";
		}
		echo json_encode ( $response );
	}
	
	/*
	 * public function do_upload()
	 * {
	 * $config['upload_path'] = './uploads/';
	 * $config['allowed_types'] = 'gif|jpg|png';
	 * $config['max_size'] = 100;
	 * $config['max_width'] = 1024;
	 * $config['max_height'] = 768;
	 *
	 * $this->load->library('upload', $config);
	 *
	 * if ( ! $this->upload->do_upload('userfile'))
	 * {
	 * $error = array('error' => $this->upload->display_errors());
	 * print_r($error);
	 * // $this->load->view('upload_form', $error);
	 * }
	 * else
	 * {
	 * $data = array('upload_data' => $this->upload->data());
	 * echo "success";
	 * //$this->load->view('upload_success', $data);
	 * }
	 * }
	 */
}
