<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : API management service
 * this service class is responsible of all the application logic 
 * related to API
 */

 class Api_service extends CI_Model
 {
	 public function __construct() {
            parent::__construct();
            $this->load->model('api/Api_dao');
            include_once './application/objects/Response.php';
      }

      /**
       * @author : Anjani Kumar Gupta
       * Date: 10th Nov 2016
       * Method: login
       * Description: validate credentials
       */
	
		public function login($email,$password,$deviceType,$deviceId,$fcmRegId)
		{
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$isUserExist = $this->isUserExist($email);
				if($isUserExist->getStatus()==1){
					
					$user = $isUserExist->getObjArray()[0];
					if($user->status==ACTIVE){
						if(md5($password)==$user->password){
							 $updateDeviceData = $apiDao->updateDeviceData($user->id,$deviceType,$deviceId,$fcmRegId);
							 if($updateDeviceData){
							 	$response->setStatus ( 1 );
							 	$response->setMsg ( "Valid user" );
							 	$response->setObjArray ( $user );
							 }else {
							 	$response->setStatus ( 0 );
							 	$response->setMsg ( "Database error" );
							 	$response->setObjArray ( NULL );
							 }
						}else {
							$response->setStatus ( 0 );
							$response->setMsg ( "Invalid credentials" );
							$response->setObjArray ( NULL );
						}
					}else {
						$response->setStatus ( 2 );
						$response->setMsg ( "Inactive user" );
						$response->setObjArray ( NULL );
					}
				}else{
					$response = $isUserExist;
				}
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		public function isUserExist($email)
		{
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->isUserExist($email);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: addLeads
		 * Description: add leads
		 */
		public function addLeads($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->addLeads($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: getCategory
		 * Description: get category
		 */
		public function getCategory(){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getCategory();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: getProduct
		 * Description: get product
		 */
		public function getProduct($catId){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getProduct($catId);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: getCluster
		 * Description: get cluster
		 */
		public function getCluster(){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getCluster();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: getBranch
		 * Description: get branch
		 */
		public function getBranch($clusterId){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getBranch($clusterId);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: getStatus
		 * Description:  All Status shown here
		 */
		public function getStatus(){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getStatus();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: changeStatus
		 * Description: Change Status
		 */
		public function changeStatus($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->changeStatus($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: addMeeting
		 * Description: add meeting
		 */
		public function addMeeting($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->addMeeting($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
			/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: updateLeadDetails
		 * Description: update details leads
		 */
		public function updateLeadDetails($data,$id)
		{
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->updateLeadDetails($data,$id);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: getAllMeeting
		 * Description:  All meeting of a sales person shown here
		 */
		public function getAllMeeting($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getAllMeeting($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 22th Nov 2016
		 * Method: getLeadsSummary
		 * Description:  Here we get how many leads are available 
		 */
		public function getLeadsSummary()
		{
			$response=new $response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getLeadsSummary();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 22th Nov 2016
		 * Method: getProcessedSummary
		 * Description:  how many leads are processed
		 */
		public function getProcessedSummary()
		{
			$response=new response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getProcessedSummary();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		/**
		 * @author : Nishant Singh
		 * Date: 22th Nov 2016
		 * Method: picture()
		 * Description: adding image
		 */
		public function picture()
		{
			$config['upload_path']   = './uploads';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']      = 100;
			$config['max_width']     = 1024;
			$config['max_height']    = 768;
			$this->load->library('upload', $config);
			 
			if ( ! $this->upload->do_upload('businessCard')) {
		
				$error = array('error' => $this->upload->display_errors());
				//$this->load->view('upload_form', $error);
			}
			 
			else {
				$data = array('upload_data' => $this->upload->data());
				return $data;
			}
		}
		
		
 }
 ?>