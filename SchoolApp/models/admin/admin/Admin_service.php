<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : API management service
 * this service class is responsible of all the application logic 
 * related to API
 */

 class Admin_service extends CI_Model
 {
	 public function __construct() {
            parent::__construct();
            $this->load->model('admin/admin/Admin_dao');
            include_once './application/objects/Response.php';
      }

      /**
       * @author : Anjani Kumar Gupta
       * Date: 10th Nov 2016
       * Method: login
       * Description: validate credentials
       */
	
		public function login($email,$password,$deviceType,$deviceId,$fcmRegId)
		{
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$isUserExist = $this->isUserExist($email);
				if($isUserExist->getStatus()==1){
					
					$user = $isUserExist->getObjArray()[0];
					if($user->status==ACTIVE){
						if(md5($password)==$user->password){
							 $updateDeviceData = $apiDao->updateDeviceData($user->id,$deviceType,$deviceId,$fcmRegId);
							 if($updateDeviceData){
							 	$response->setStatus ( 1 );
							 	$response->setMsg ( "Valid user" );
							 	$response->setObjArray ( $user );
							 }else {
							 	$response->setStatus ( 0 );
							 	$response->setMsg ( "Database error" );
							 	$response->setObjArray ( NULL );
							 }
						}else {
							$response->setStatus ( 0 );
							$response->setMsg ( "Invalid credentials" );
							$response->setObjArray ( NULL );
						}
					}else {
						$response->setStatus ( 2 );
						$response->setMsg ( "Inactive user" );
						$response->setObjArray ( NULL );
					}
				}else{
					$response = $isUserExist;
				}
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($checkExistingUser->getMsg());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		public function isUserExist($email)
		{
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->isUserExist($email);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($checkExistingUser->getMsg());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: addLeads
		 * Description: add leads
		 */
		public function addLeads($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->addLeads($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($checkExistingUser->getMsg());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		public function checkEmail($email)
		{
			$this->db->where('email',$email);
			$result = $this->db->get('user');
			$rows = $result->num_rows ();
			if($rows === 1)
			{
				return true;
			}
			
		}
		
		public function updatePassword ( $random_code, $email )
		{
			$data['password'] = md5($random_code);
			$this->db->where('email',$email);
			$update = $this->db->update ('user', $data );
			
			if($update)
			{
				return true;
			}
			
			else 
			{
				return false;
			}
		}
		
		public function GetUserProfile($userid)
		{
			$this->db->where('id',$userid);
			$result = $this->db->get('user');
			$rows = $result->row();
			return $rows;
		}
		
		public function update_profile($data,$userid)
		{
			$this->db->where('id',$userid);
			$result = $this->db->update('user',$data);
			if($result)
			{
				return true;
			}
			
			
		}
		
		public function update_password($data,$userid)
		{
			$this->db->where('id',$userid);
			$result = $this->db->update('user',$data);
			if($result)
			{
				return true;
			}
		}
	
 }
 
 
 
 
 ?>