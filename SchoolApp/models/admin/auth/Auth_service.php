<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : User management service
 * this service class is responsible of all the application logic 
 * related to users
 */

 class Auth_service extends CI_Model
 {
	 public function __construct() {
            parent::__construct();
            $this->load->model('admin/auth/Auth_dao');
            include_once './application/objects/Response.php';
      }

      /**
       * @author : Anjani Kumar Gupta
       * Date: 10th Nov 2016
       * Method: login
       * Description: validate credentials
       */
	
		public function login($userId,$password)
		{
			$response = new Response();
			try {
				$apiDao = new Auth_dao();
				$isUserExist = $this->isUserExist($userId);
				if($isUserExist->getStatus()==1){
					
					$user = $isUserExist->getObjArray();
					if($user->status==ACTIVE && $user->role_id==USER_ROLE_ADMIN || $user->role_id==USER_ROLE_HO || $user->role_id==USER_ROLE_CMO){
						if(md5($password)==$user->password){
							 //create session
							$createSession = $this->createSession($user);
						 	$response->setStatus ( 1 );
						 	$response->setMsg ( "Valid credentials." );
						 	$response->setObjArray ( $user );
						}else {
							$response->setStatus ( 0 );
							$response->setMsg ( "Invalid credentials." );
							$response->setObjArray ( NULL );
						}
					}else {
						$response->setStatus ( 2 );
						$response->setMsg ( "Inactive user or you are not an autherized user." );
						$response->setObjArray ( NULL );
					}
				}else{
					$response = $isUserExist;
				}
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($checkExistingUser->getMsg());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		public function isUserExist($userId)
		{
			$response = new Response();
			try {
				$authDao = new Auth_dao();
				$response = $authDao->isUserExist($userId);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($checkExistingUser->getMsg());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		public function createSession($user){
			$prev = $user->privileges;
			$p = array();
			foreach ($prev as $val){
				array_push($p, $val->id);
			}
			$data = array(
					'id'  => $user->id,
					'firstname'    => $user->firstname,
					'lastname'     => $user->lastname,
					'email'    => $user->email,
					'username'    => $user->username,
					'role_id'    => $user->role_id,
					'privileges'    => $p,
					'status'    => $user->status,
					'logged_in' => TRUE,
					'summary' => $user->summary
			);
			
			$this->session->set_userdata($data);
		}
		
 }
 
 
 
 
 ?>